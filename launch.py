#!/usr/bin python3
import os
import re
import shutil
import subprocess
import time

CONFIG_FILE = os.environ["ARMA_CONFIG"]
KEYS = os.environ["STEAMAPPDIR"] + "/keys"

if not os.path.exists(KEYS) or not os.path.isdir(KEYS):
    if os.path.exists(KEYS):
        os.remove(KEYS)
    os.makedirs(KEYS)

subprocess.call(
    [os.environ["STEAMCMDDIR"] + "/steamcmd.sh", "+login", os.environ["STEAM_USER"], os.environ["STEAM_PASSWORD"],
     "+force_install_dir", os.environ["STEAMAPPDIR"], "+app_update", os.environ["STEAMAPPID"], "validate", "+quit"])


def mods(d):
    launch = "\""
    mods = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    for m in mods:
        launch += m + ";"
        keysdir = os.path.join(m, "keys") #BUGBUG: Make sure that mod keys directories are lowercase, this is case sensitive
        if os.path.exists(keysdir):
            keys = [os.path.join(keysdir, o) for o in os.listdir(keysdir) if
                    os.path.isdir(os.path.join(keysdir, o)) == False]
            for k in keys:
                shutil.copy2(k, KEYS)
        else:
            print("Missing keys:", keysdir)
    return launch + "\""


launch = "{} -filepatching -mod={} -world={}".format(os.environ["STEAMAPPDIR"] + os.environ["ARMA_BINARY"],
                                       mods(os.environ["STEAMAPPDIR"] + '/mods'),
                                       os.environ["ARMA_WORLD"])

clients = int(os.environ["HEADLESS_CLIENTS"])

print("Headless Clients:", clients)

if clients != 0:
    with open(os.environ["STEAMAPPDIR"] + "/configs/{}".format(CONFIG_FILE)) as config:
        data = config.read()
        regex = r"(.+?)(?:\s+)?=(?:\s+)?(.+?)(?:$|\/|;)"

        config_values = {}

        matches = re.finditer(regex, data, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            config_values[match.group(1).lower()] = match.group(2)

        if not "headlessclients[]" in config_values:
            data += "\nheadlessclients[] = {\"127.0.0.1\"}\n"
        if not "localclient[]" in config_values:
            data += "\nlocalclient[] = {\"127.0.0.1\"}\n"

        with open("/tmp/arma3.cfg", "w") as tmp_config:
            tmp_config.write(data)
        launch += " -config=\"/tmp/arma3.cfg\""

    client_launch = launch
    print("client_launch=" + client_launch)
    client_launch += " -client -connect=127.0.0.1"
    if "password" in config_values:
        client_launch += " -password={}".format(config_values["password"])

    for i in range(0, clients):
        print("LAUNCHING ARMA CLIENT {} WITH".format(i), client_launch)
        subprocess.Popen(client_launch, shell=True)

else:
    launch += " -config=\"{}/configs/{}\"".format(os.environ["STEAMAPPDIR"], CONFIG_FILE)

launch += " -port={} -name=\"{}\" -profiles=\"{}/configs/profile\"".format(os.environ["PORT"],
                                                                            os.environ["ARMA_PROFILE"],
                                                                            os.environ["STEAMAPPDIR"])

launch += " -serverMod={}".format(os.environ["ARMA_SERVERMOD"])

print("LAUNCHING ARMA SERVER WITH COMMAND", launch, flush=True)
os.system(launch)

# LAUNCHING ARMA SERVER WITH COMMAND /home/steam/arma3-dedicated/arma3server_x64 -filepatching
# -mod="/home/steam/arma3-dedicated/mods/@Enhanced
# Movement;/home/steam/arma3-dedicated/mods/@CBA_A3;/home/steam/arma3-dedicated/mods/@Zeus
# Enhanced;/home/steam/arma3-dedicated/mods/@Zeus Enhanced - ACE3 Compatibility;" -world=empty
# -config="/home/steam/arma3-dedicated/configs/server.cfg" -port=2302 -name="main"
# -profiles="/home/steam/arma3-dedicated/configs/profile" -serverMod=@Apex

FROM cm2network/steamcmd:root

LABEL maintainer="malk0lm@pm.me"

ENV STEAMAPPID 233780
ENV STEAMAPP arma3
ENV STEAMAPPDIR "${HOMEDIR}/${STEAMAPP}-dedicated"

VOLUME ${STEAMAPPDIR}

COPY --chown=${USER}:${USER} *.pbo ${STEAMAPPDIR}/mpmissions/
COPY --chown=${USER}:${USER} servermods ${STEAMAPPDIR}
COPY --chown=${USER}:${USER} config ${STEAMAPPDIR}
COPY --chown=${USER}:${USER} server.cfg ${STEAMAPPDIR}/configs/
COPY --chown=${USER}:${USER} server.Arma3Profile ${STEAMAPPDIR}/configs/profile/home/main/main.Arma3Profile
COPY --chown=${USER}:${USER} basic.cfg ${STEAMAPPDIR}/configs/basic.cfg
COPY --chown=${USER}:${USER} mods ${STEAMAPPDIR}/mods
COPY --chown=${USER}:${USER} launch.py ${STEAMAPPDIR}/launch.py

RUN set -x \
	&& apt-get update \
	&& apt-get install -y --no-install-recommends --no-install-suggests \
		wget=1.20.1-1.1 python3 net-tools \
	&& chmod -R 755 "${STEAMAPPDIR}" \
	&& chown -R "${USER}:${USER}" "${STEAMAPPDIR}" \
	&& rm -rf /var/lib/apt/lists/*

ENV ARMA_BINARY="/arma3server_x64"
ENV ARMA_CONFIG=server.cfg
ENV ARMA_PROFILE=main
ENV ARMA_WORLD=empty
ENV HEADLESS_CLIENTS=0
ENV PORT=2302
ENV STEAM_USER="<USER>"
ENV STEAM_PASSWORD="<PASSWORD>"
ENV ARMA_SERVERMOD="@Apex"

USER ${USER}

WORKDIR ${STEAMAPPDIR}

CMD [ "sh", "-c", "python3 $STEAMAPPDIR/launch.py" ]

# Expose ports
EXPOSE 2302/udp
EXPOSE 2303/udp
EXPOSE 2304/udp
EXPOSE 2305/udp
EXPOSE 2306/udp
